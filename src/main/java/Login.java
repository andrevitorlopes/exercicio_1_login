
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author andre
 */

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class Login extends HttpServlet{
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
        String perfil = "";
        switch(request.getParameter("perfil")){
            case "1": perfil = "Cliente";
                    break;
            case "2": perfil = "Gerente";
                    break;
            case "3": perfil = "Administrador";
                    break;
                
        }
        
        try (PrintWriter out = response.getWriter()) {
            if(login.equals(senha)){
                response.sendRedirect(request.getContextPath() + "/sucesso?login="
                        +login+"&perfil="+ perfil);
            }
            else{
                response.sendRedirect(request.getContextPath() + "/erro.xhtml");
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
