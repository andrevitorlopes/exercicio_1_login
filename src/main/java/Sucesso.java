/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Sucesso", urlPatterns = {"/sucesso"})
public class Sucesso extends HttpServlet{
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String login = request.getParameter("login");
        String perfil = request.getParameter("perfil");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" "
                    + "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Acesso permitido</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("Acesso a " + request.getParameter("perfil") + " " 
                    + request.getParameter("login") + " permitido");
            out.println("</body>");
            out.println("</html>");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}